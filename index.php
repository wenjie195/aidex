<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/" />
<meta property="og:title" content="ASEAN Investments Digital Exchange | Aidex" />
<title>ASEAN Investments Digital Exchange | Aidex</title>
<link rel="canonical" href="https://aidex.sg/" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
    <div class="landing-first-div width100 overflow">
        <div class="first-right-div web-none">
        	<img src="img/ai.jpg" alt="Artificial Intelligence (AI)" title="Artificial Intelligence (AI)" class="ai-img">
        </div>    
    	<div class="first-left-div white-text overflow">
        	<h3 class="first-banner-h3 white-text">WELCOME TO</h3>
            <img src="img/white-big-logo.png" alt="AIDEX" title="AIDEX" class="banner-logo">
            <p class="first-banner-p white-text">The power of AIDEX artificial intelligence that revolutionizes ASEAN investments Digital Exchange. Our system allows clients to easily access money and generate returns with user-friendly platform.</p>
        	<a href="#" class="open-signup"><div class="blue-bg rounded-200-button white-text">Get Started</div></a>
        </div>
        <div class="first-right-div mobile-none">
        	<img src="img/ai.jpg" alt="Artificial Intelligence (AI)" title="Artificial Intelligence (AI)" class="ai-img">
        </div>
    
    </div>
    <div class="clear"></div>
    <div class="second-four-div width100 same-padding">
    	<img src="img/question.png" class="line-icon" alt="Question" title="Question">
        <p class="bold-subtitle-p separate-distance">
        	Join us now to get started. 
        </p>
    	<div class="clear"></div>
        <div class="four-div">
        	<img src="img/investment.png" class="four-div-img" alt="Investment" title="Investment">
            <p class="four-div-p">
            	You have money to invest but you know little about crypto. You are not sure where to start.
            </p>
        </div>
        <div class="four-div four-mid-left-div">
        	<img src="img/digital-portfolio.png" class="four-div-img" alt="Digital Portfolio" title="Digital Portfolio">
            <p class="four-div-p">
            	You are increasing your exposure and investment in the digital portfolio.
            </p>        
        </div>
        <div class="four-div-tempo-clear"></div>
        <div class="four-div four-mid-right-div">
        	<img src="img/search.png" class="four-div-img" alt="Search" title="Search">
            <p class="four-div-p">
            	You are searching for a digital exchange that provides excellent services and something unique.
            </p>        
        </div>
        <div class="four-div">
        	<img src="img/company.png" class="four-div-img" alt="Company" title="Company">
            <p class="four-div-p">
            	You are a company or a group aiming for customization services from the digital exchange.
            </p>
        </div>                
    </div>
	<div class="clear"></div>
    <div class="width100 same-padding padding-top-50">
    	<h2 class="line-h2"><img src="img/cryptocurrency.png" class="line-icon line-icon-spacing" alt="Cryptocurrency" title="Cryptocurrency"></h2>
        <div class="clear"></div>
        <div class="two-left-visual-div two-left float-left">
        	<img src="img/cryptocurrency-help.png" class="width100" alt="Help You with Cryptocurrency" title="Help You with Cryptocurrency">
        </div>
        <div class="two-right-content-div two-right float-right">
        	<p class="bold-subtitle-p two-content-top-p">
            	New to cryptocurrency? No worry, most of the people can do it, no doubt you can also do it. More importantly, we're going to help you from scratch. 
            </p>
            <p class="two-content-p">
            	We have compiled the best guides by the expert to assist you to understand what is digital assets and how it is going to affect our future view towards money. Once you get the fundamental part, we will guide you through the technical part later one! Stay with us.
            </p>
            <a>
            	<div class="full-width-btn blue-bg blue-btn-hover">
                	Learn More About Cryptocurrency
                </div>
            </a>
        </div>
        <div class="clear"></div>
        <div class="two-left-visual-div two-right float-right">
        	<img src="img/crptocurrency-security.png" class="width100" alt="Security" title="Security">
        </div>
        <div class="two-right-content-div two-left float-left">
        	<p class="bold-subtitle-p two-content-top-p">
            	AIDEX equip you with the ammo to stay in the battleground, and keep your safety regardless of your capabilities.
            </p>
            <p class="two-content-p">
            	We understand our client’s feelings and are looking for the best experience and value in the cryptocurrency exchange. Thus, we created a platform that is intuitive and convenient from deposits, trade to withdrawals. 
            </p>
            <a>
            	<div class="full-width-btn blue-bg blue-btn-hover">
                	Know More About Our Features
                </div>
            </a>
        </div>
        <div class="clear"></div>
        <div class="two-left-visual-div two-left float-left">
        	<img src="img/high-security.png" class="width100" alt="High Security" title="High Security">
        </div>
        <div class="two-right-content-div two-right float-right">
        	<p class="bold-subtitle-p two-content-top-p">
            	We treat security with upmost important in our organization.
            </p>
            <p class="two-content-p">
            	To let our customer trade worry-free, we always on our guard and take necessary measures to protect your digital assets in our safe. Not only we have followed the best practices available in the market, but we also take one-step forward to stay ahead of our competitors in terms of security.
            </p>
            <a>
            	<div class="full-width-btn blue-bg blue-btn-hover long-blue-div">
                	Learn More About Our Security Capabilities
                </div>
            </a>
        </div>        
        <div class="clear"></div>
        <div class="two-left-visual-div two-right float-right">
        	<img src="img/cryptocurrency-assist.png" class="width100" alt="Assist" title="Assist">
        </div>
        <div class="two-right-content-div two-left float-left">
        	<p class="bold-subtitle-p two-content-top-p">
            	We will be here when you need our assist!
            </p>
            <p class="two-content-p">
            	Indeed, it is painful that exchanges have slow responsive customer service. In AIDEX, we deliver our top-notch services to you in the shortest possible time frame so that you'll gain the most out of our platform! 
            </p>
            <a href="#">
            	<div class="full-width-btn blue-bg blue-btn-hover">
                	Visit Our Support Center
                </div>
            </a>
        </div>        
        
        
                
    </div>
    <div class="clear"></div>
    <div class="width100 same-padding text-center some-spacing" id="email-div">
    	<img src="img/stay-tune.png" class="line-icon" alt="Stay Tuned" title="Stay Tuned" >
        <p class="bold-subtitle-p">
        	Stay Tuned!
        </p> 
        <div class="two-left-visual-div two-left float-left margin-control">
        	<img src="img/member.png" class="width100" alt="Stay Tuned!" title="Stay Tuned!">
        </div>
        <div class="two-right-content-div two-right float-right text-left margin-control">
        	<p class="stay-tune-p">
            	Our website is COMING SOON.<br>We're working our best to create a website beyond your imagination.<br>
                We'll notify you once we are ready to launch the site.<br><br>
                If you have any query, please contact us at <b>support@aidex.sg</b><br>
                We're more than happy to receive your suggestion for our future improvement.<br><br><br>
                Best,<br>
                The AIDEX Team
            </p>
        	<!-- <form> -->
            <form action="utilities/addNewEmailFunction.php" method="POST">
             	<div class="input-div">
                    <p class="input-top-text">Email</p>
                    <!-- <input class="aidex-input clean" type="email" placeholder="Type Your Email"> -->
                    <input class="aidex-input clean" type="text" placeholder="Type Your Email" id="submit_email" name="submit_email" required>
                </div>   
        
                <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top" name="refereeButton">Submit</button>
                                                        
            </form>
        </div>          
           
    </div>

    <!-- <div class="second-four-div width100 same-padding form-div" id="signup-div">
    	<img src="img/sign-up-icon.png" class="line-icon" alt="Sign Up" title="Sign Up" >
        <p class="bold-subtitle-p">
        	Sign Up Now
        </p>
        <div class="two-left-visual-div two-left float-left margin-control">
        	<img src="img/sign-up.png" class="width100" alt="Sign Up" title="Sign Up">
        </div>
        <div class="two-right-content-div two-right float-right text-left margin-control">
            <form action="utilities/newUserRegisterFunction.php" method="POST">
            	<div class="input-div">
                    <p class="input-top-text">Name</p>
                    <input class="aidex-input clean" type="text" placeholder="Type Your Name Here" id="register_fullname" name="register_fullname" required>
                </div>
             	<div class="input-div">
                    <p class="input-top-text">Username</p>
                    <input class="aidex-input clean" type="text" placeholder="Type Your Username" id="register_username" name="register_username" required>
                </div>  
             	<div class="input-div">
                    <p class="input-top-text">Email</p>
                    <input class="aidex-input clean" type="email" placeholder="Type Your Email" id="register_email_user" name="register_email_user" required>
                </div>   
             	<div class="input-div">
                    <p class="input-top-text">Password</p>
                    <input class="aidex-input clean" type="text" placeholder="Type Your Password" id="register_password" name="register_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover" alt="View Password" title="View Password">
                </div>                 
             	<div class="input-div">
                    <p class="input-top-text">Comfirm Password</p>
                    <input class="aidex-input clean" type="text" placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover" alt="View Password" title="View Password">
                </div>  
             	<div class="input-div">
                    <p class="input-top-text">Country</p>
                    <select class="aidex-input clean">
                    	<option>Singapore</option>
                        <option>Malaysia</option>
                    </select>
                </div> 
                <div class="input-div">
                    <p class="input-top-text">Country</p>
                    <input class="aidex-input clean" type="text" placeholder="Country" id="register_nationality" name="register_nationality" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover" alt="View Password" title="View Password">
                </div>
                <div class="input-div">
                	<p class="input-top-text">Contact Number</p>
                    <input class="aidex-input clean mobile-input" type="text" placeholder="Contact Number" id="register_phone" name="register_phone" required>
                </div>  

                <div class="clear"></div>

                <input type="checkbox" class="aidex-checkbox"><label class="aidex-label">By ticking I agree to the <a href="#" class="blue-link">Terms of Services</a></label>
                
                <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top" name="refereeButton">Create Account</button>
            </form>
        </div>  
        
        <div class="clear"></div>

        <div class="separate-distance2" id="login-div"></div>
    	<img src="img/sign-in-icon.png" class="line-icon" alt="Sign In" title="Sign In">
        <p class="bold-subtitle-p" >
        	Already created an account? Sign in instead.
        </p>        
        <div class="two-left-visual-div two-right float-right margin-control">
        	<img src="img/sign-in.png" class="width100" alt="Sign In" title="Sign In">
        </div>
        <div class="two-right-content-div two-left float-left margin-control text-left">
        <form action="utilities/loginFunction.php" method="POST">
             	<div class="input-div">
                    <p class="input-top-text">Username</p>
                    <input class="aidex-input clean" type="text" placeholder="Type Your Username" id="username" name="username" required> 
                </div>
             	<div class="input-div">
                    <p class="input-top-text">Password</p>
                    <input class="aidex-input clean" type="password" placeholder="Type Your Password" id="password" name="password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover" alt="View Password" title="View Password">
                </div> 

                <div class="clear"></div>

                <input type="checkbox" class="aidex-checkbox"><label class="aidex-label">Remember Me</label>

                <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top dual-a-btm" name="loginButton">Sign In</button> 

                <a  class="open-forgot blue-link forgot-a">Forgot Password</a> 
                <a class="open-forgotusername blue-link forgot-u-a">Forgot Username</a> 
       	</form>                  
        </div>         
	</div> -->
    
    <div class="clear"></div>
    <div class="spacing-div"></div>

<!-- CSS -->
<style>
.food-gif{
	width:100px;
	position:absolute;
	top:calc(50% - 150px);
	text-align:center;
}
.center-food{
	width:100%;
	text-align:center;
	margin-left:-50px;}
#container{
	margin-top:-20px;}
#overlay{
  position:fixed;
  z-index:99999;
  top:0;
  left:0;
  bottom:0;
  right:0;
  background:#7cd1d1;
  /*background: -moz-linear-gradient(left, #a9151c 0%, #d60d26 100%);
  background: -webkit-linear-gradient(left, #a9151c 0%,#d60d26 100%);
  background: linear-gradient(to right, #a9151c 0%,#d60d26 100%);*/
  transition: 1s 0.4s;
}
#progress{
  height:1px;
  background:#fff;
  position:absolute;
  width:0;
  top:50%;
}
#progstat{
  font-size:0.7em;
  letter-spacing: 3px;
  position:absolute;
  top:50%;
  margin-top:-40px;
  width:100%;
  text-align:center;
  color:#fff;
}
@media all and (max-width: 500px){
.food-gif{
	width:60px;
	top:calc(50% - 120px);
	text-align:center;
}
.center-food{
	margin-left:-30px;}	

}
</style>
<?php include 'js.php'; ?>
</body>
</html>