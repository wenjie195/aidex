<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" />
    <meta property="og:title" content="添加下线 | Q联盟" />
    <title>添加下线 | Q联盟</title>
    <meta property="og:description" content="Q联盟" />
    <meta name="description" content="Q联盟" />
    <meta name="keywords" content="Q联盟, League Q,etc">
    <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" />

</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

    <h1 class="h1-title h1-before-border">Registration</h1>

    <div class="clear"></div>
    
        <form  class="edit-profile-div2" action="utilities/addNewRefereeFunction.php" method="POST">
            <table class="edit-profile-table password-table">
                <tr class="profile-tr">
                    <td class="">Username</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Username" id="register_username" name="register_username" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td class="">Fullname</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Fullname" id="register_fullname" name="register_fullname" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Phone</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Phone" id="register_phone" name="register_phone" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Email</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="email" placeholder="Email" id="register_email_user" name="register_email_user" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Nationality</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Nationality" id="register_nationality" name="register_nationality" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Password</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Password" id="register_password" name="register_password" required>
                    </td>
                </tr>

                <tr class="profile-tr">
                    <td>Retype Password</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3">
                        <input class="clean edit-profile-input" type="text" placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required>
                    </td>
                </tr>

            </table>

            <div class="clear"></div>

            <button class="confirm-btn text-center white-text clean gold-button"name="refereeButton">Submit</button>
        </form>
        
        <a href="logout.php"  class="menu-padding"><img src="img/logout-white.png" class="cart-img" alt="Logout" title="Logout"></a>

</div>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "成功注册新用户！";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "注册新用户失败！";
        }
        
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("通告 !! ","'.$messageType.'");
    </script>
    ';   
}
?>

</body>
</html>