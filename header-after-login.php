<header id="header" class="header header--fixed same-padding header1 menu-bg" role="banner">
    <div class="big-container-size hidden-padding">
    	<div class="left-logo-div float-left hidden-logo-padding">
    		<img src="img/white-logo.png" class="logo-img web-logo" alt="AIDEX" title="AIDEX">
            <img src="img/aidex.png" class="logo-img mobile-logo" alt="AIDEX" title="AIDEX">
           
   		</div>
        <div class="right-menu-div float-right display-block-forever">
            <a href="logout.php" class="white-text opacity-hover open-login">Logout</a>  
            
        </div>
	</div>

</header>