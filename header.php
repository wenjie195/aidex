<header id="header" class="header header--fixed same-padding header1 menu-white" role="banner">
    <div class="big-container-size hidden-padding">
    	<div class="left-logo-div float-left hidden-logo-padding">
    		<img src="img/white-logo.png" class="logo-img web-logo" alt="AIDEX" title="AIDEX">
            <img src="img/aidex.png" class="logo-img mobile-logo" alt="AIDEX" title="AIDEX">
           
   		</div>
        <div class="middle-menu-div web-menu">
        	<a class="white-text menu-padding opacity-hover">Features</a>
            <a class="white-text menu-padding opacity-hover">Markets</a>
            <a class="white-text opacity-hover">Support</a>        	
        </div>
        <div class="right-menu-div float-right">
            <a href="#email-div" class="white-text menu-padding opacity-hover open-login web-menu">Login</a>
            <a href="#email-div" class="white-text opacity-hover open-signup web-menu">Sign Up</a>
                           	<div id="dl-menu" class="dl-menuwrapper">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                   <li><a>Features</a></li>
                                  <li><a>Markets</a></li>
                                  <li><a>Support</a></li>
                                  <li><a class="open-login" >Login</a></li>
                                  <li><a class="open-signup">Product</a></li>
                                </ul>
							</div><!-- /dl-menuwrapper -->           
            
        </div>
	</div>

</header>