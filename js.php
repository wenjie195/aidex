<div class="clear"></div>
<div class="width100 same-padding footer-div blue-bg"><p class="white-text footer-p text-center">@2020 AIDEX, All Rights Reserved.</p></div>
<!-- Forgot Password Modal -->
<div id="forgot-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-forgot">&times;</span>
    <h2 class="blue-h2">Forgot Password</h2>
    <form class="login-form" method="POST" >
    	<div class="input-popout-div">
        	<input class="aidex-input clean pop-out-input" type="email" placeholder="Type Your Email Here" required name="forgotPassword_email">
        </div>
        <div class="clear"></div>
        <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top">Submit</button>
        
    </form>
  </div>

</div>
<!-- Forgot Username Modal -->
<div id="forgotusername-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-forgotusername">&times;</span>
    <h2 class="blue-h2">Forgot Username</h2>
    <form class="login-form" method="POST" >
    	<div class="input-popout-div">
        	<input class="aidex-input clean pop-out-input" type="email" placeholder="Type Your Email Here" required name="forgotPassword_email">
        </div>
        <div class="clear"></div>
        <button class="full-width-btn blue-bg blue-btn-hover long-blue-div clean-button clean extra-margin-top">Submit</button>
        
    </form>
  </div>

</div>



<script src="js/jquery-3.2.0.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
<script src="js/headroom.js"></script>
<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
        <script>
	$(document).ready(function() {
	var s = $(".menu-white");
	var pos = s.position();					   
	$(window).scroll(function() {
		var windowpos = $(window).scrollTop();
		if (windowpos >= pos.top & windowpos >=170) {
			s.addClass("menu-bg");
		} else {
			s.removeClass("menu-bg");	
		}
		});
	});
	if (window.matchMedia('screen and (max-width: 1200px)').matches) {
	$(document).ready(function() {
	var s = $(".menu-white");
	var pos = s.position();					   
	$(window).scroll(function() {
		var windowpos = $(window).scrollTop();
		if (windowpos >= pos.top & windowpos >=100) {
			s.addClass("menu-bg");
		} else {
			s.removeClass("menu-bg");	
		}
		});
	});		
		}
	</script> 
    
<!-- Responsive Menu --->
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>
        
        
<!--- Modal Box --->
<script>
var forgotmodal = document.getElementById("forgot-modal");
var forgotusernamemodal = document.getElementById("forgotusername-modal");
var openforgot = document.getElementsByClassName("open-forgot")[0];
var openforgotusername = document.getElementsByClassName("open-forgotusername")[0];
var closeforgot = document.getElementsByClassName("close-forgot")[0];
var closeforgotusername = document.getElementsByClassName("close-forgotusername")[0];
if(openforgot){
openforgot.onclick = function() {
  forgotmodal.style.display = "block";
}
}
if(openforgotusername){
openforgotusername.onclick = function() {
  forgotusernamemodal.style.display = "block";
}
}
if(closeforgot){
closeforgot.onclick = function() {
  forgotmodal.style.display = "none";
}
}
if(closeforgotusername){
closeforgotusername.onclick = function() {
  forgotusernamemodal.style.display = "none";
}
}
window.onclick = function(event) {
  if (event.target == forgotmodal) {
    forgotmodal.style.display = "none";
  }
  if (event.target == forgotusernamemodal) {
    forgotusernamemodal.style.display = "none";
  }  
}
</script>