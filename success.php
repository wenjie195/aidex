<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://aidex.sg/success.php" />
<meta property="og:title" content="Success | Aidex" />
<title>Success | Aidex</title>
<link rel="canonical" href="https://aidex.sg/success.php" />
<meta http-equiv="refresh" content="3;url=https://aidex.sg/" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="width100 text-center">
    	<img src="" alt="Success" title="Success">
    	<p class="email-msg-p">Thank you! Your email has been submitted!</p>
    </div>
    <div class="clear"></div>
    <div class="spacing-div"></div>

<!-- CSS -->

<?php include 'js.php'; ?>
</body>
</html>